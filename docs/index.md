# Python Logging, Remastered

This package does not necessarily seek to _replace_ the built-in
[logging][py-logging] package, but offer an alternative with a more
streamlined feature set that isn't cluttered with much of the legacy support
for older versions of Python.
See the [compatibility guidelines][compat] section for more details.

For more information about the project, such as its licensing or how to
contribute to its development please see the links under the **About**
section.

[py-logging]: https://docs.python.org/3/library/logging.html
[compat]: /about/contributing/#python-version

## Related Pages

* [GitLab Repository](https://gitlab.com/kyberdin/relogged)
* PyPi Page _(TBD)_
