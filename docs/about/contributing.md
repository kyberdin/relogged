# Contributing

Anyone is welcome to contribute to this project to make it better!
Logging is awesome, and it would be great to expand this library to be helpful
to everyone who wants it, and represent the collaborative desires of logging
fans.

Some of the best ways to contribute are:

- [Pull Requests](#pull-requests)
- [Documentation updates](#documentation)
- [Bug reports](#reporting-an-issue)

## Code of Conduct

Everyone interacting in the `relogged` project is expected to abide by
the [PyPA Code of Conduct].

[PyPA Code of Conduct]: https://www.pypa.io/en/latest/code-of-conduct/

## Compatibility

### Python Version

As of release [0.1.0], no effort has been added to make this library compatible
with **Python-2.7**.
While the maintainers are aware that there are many, many Python 2.7 users and
projects out there, the point of the project is to give the `logging` package
a fresh lift for moving forward in Python.
Considering that 2.7 has been [deprecated as of 2020][py27-deprecation],
supporting it is out of scope for initial development.

Contributing to the project by adding such support is welcome by anyone
interested in converting it.
Please file the appropriate Issue or Pull Request over such work to be reviewed.

[py27-deprecation]: https://www.python.org/dev/peps/pep-0373/

### Windows

Due to the author's general dislike of Windows, especially regarding software
development, no thoughts towards compatibility are implemented as of release
[0.1.0].

Given the cross-platform nature of Python in general, however, this doesn't
mean that this package won't work on it; the author is just not going out of
their way to guarantee it.

[0.1.0]: release-notes.md

## Reporting an Issue

Please include as much detail as you can, as well as your Python version and
operating system.

## Development

Fork the repository and dev away! It is _highly_ recommended to use a
[virtual environment][virtualenv] for isolating package-related dependencies to
the development of the project.

[virtualenv]: https://virtualenv.pypa.io/en/latest/userguide.html

For CI purposes, package requirements have been split up into separate files
within the `requirements` directory, which can be installed individually with
`pip`.
Developing on this project, however, requires all of these to be installed.

Installing all dependencies is as easy as pointing `pip` at the root
`requirements.txt` file:

=== "Virtualenv"
    ```
    $ pip install -r requirements.txt
    ```

=== "System"
    ```
    $ python3 -m pip install --user -r requirements.txt
    ```

### Documentation

When creating documentation, please try to follow the same syntax as Markdown
usage as the rest of documentation files so that its source blends together
seamlessly.

This project uses [MkDocs].
As such, the Markdown usage is oriented towards the use of some MkDocs
extensions, as listed in the `mkdocs.yml` configuration file.
If some use seems weird, consult the documentation of the extension to see what
was the usage was implementing.
Please also refer to this file for all other site layout and settings.

[MkDocs]: https://www.mkdocs.org/

## Testing

Tests are built using [pytest].
Install the package into your virtual environment of system, and then run the
all of the tests from the repository root:

```
$ pytest test -v
```

If you are using `print` for debugging test issues, append `-s` to the `pytest`
command.

[pytest]: https://pytest.org/en/latest/

## Pull Requests

Once you are happy with your changes or you are ready for some feedback, push it
and open a PR for a maintainer to review.
Please have documentation and tests in place to verify and explain the
functionality of the new feature in detail.
