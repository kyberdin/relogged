"""
test_core.py

Core module test cases.

:author:        Stephen Stauts
:created:       16/05/2020   (DD/MM/YYYY)
:copyright:     © 2020 Stephen Stauts. All Rights Reserved.
"""

# Standard Packages
import time

# Dependency Packages
import pytest

# Local Packages and Modules
from conftest import does_not_raise
import relog


class TestLogLevel:
    """Test the logging level enumeration and class methods."""

    def test_init(self):
        """
        A class can be correctly initialized.
        """
        weight = 200
        name = 'TestLevel'
        level = relog.LogLevel(name, weight)
        assert level.weight == weight
        assert level.name == name.upper()

    def test_init_no_params(self):
        """Passing no parameters raises an Exception."""
        with pytest.raises(ValueError):
            level = relog.LogLevel()

    def test_init_bad_name(self):
        """Passing in an invalid name raises an Exception."""
        with pytest.raises(TypeError):
            level = relog.LogLevel(name=10, weight=100)

    def test_init_empty_name(self):
        """Passing in an empty name raises an Exception."""
        with pytest.raises(ValueError):
            level = relog.LogLevel(name='', weight=100)

    def test_init_bad_weight(self):
        """Passing a non-int weight raises an Exception."""
        with pytest.raises(TypeError):
            level = relog.LogLevel(name='test', weight='a')

    def test_name_setter_sets_name(self):
        """
        A new name can be assigned to the name attribute.
        """
        name = 'TestLevel'
        level = relog.LogLevel(name, 200)

        new_name = 'NewTestLevel'
        level.name = new_name
        assert level.name == new_name.upper()

    def test_weight_setter_sets_weight(self):
        """
        A new weight can be assigned to the weight attribute.
        """
        weight = 200
        level = relog.LogLevel('TestLevel', weight)

        new_weight = weight * 2
        level.weight = new_weight
        assert level.weight == new_weight


class TestLogRecord:
    """Test LogRecord methods and attribute handling."""

    def test_init(self):
        """A LogRecord can be correctly initialized to expected values."""
        level = relog.INFO
        text = 'some text'
        lr = relog.LogRecord(level, text)
        assert lr.text == text
        assert lr.level == level

    def test_init_bad_level(self):
        """
        The correct exception is raised when a `LogRecord` is created with an
        invalid logging level.
        """
        with pytest.raises(ValueError):
            lr = relog.LogRecord(9999, '')


class TestCoreFunctions:
    """Test cases for core functions."""

    def test_get_lock_existing_id(self):
        """
        Accessing a default lock correctly returns the lock without adding a
        new entry.
        """
        import _thread
        default_len = len(relog.core._d_log_locks)
        default_stdout_lock = relog.core._d_log_locks['stdout']
        lock = relog.core.get_lock('stdout')
        assert isinstance(lock, _thread.RLock)
        assert len(relog.core._d_log_locks) == default_len
        assert lock is default_stdout_lock

    def test_get_lock_new_id_returns_lock(self):
        """
        Retrieving an log lock ID that is not yet tracked returns a lock and
        adds a new entry for it in the dict of tracked logs.
        """
        import _thread
        new_id = 'newid'
        default_len = len(relog.core._d_log_locks)
        new_lock = relog.core.get_lock(new_id)
        assert isinstance(new_lock, _thread.RLock)
        assert len(relog.core._d_log_locks) == (default_len + 1)
        assert new_id in relog.core._d_log_locks
        relog.core._d_log_locks.pop(new_id)

    def test_add_log_level_zero_weight(self):
        """Passing a 0 weight raises an Exception (reserved value)."""
        with pytest.raises(ValueError):
            relog.add_log_level(name='test', weight=0)

    def test_add_log_level_existing_weight(self):
        """
        Passing a non-zero weight that is already in use raises an Exception.
        """
        with pytest.raises(ValueError):
            relog.add_log_level(name='test', weight=relog.INFO)

    def test_add_log_level_existing_name(self):
        """
        Passing a non-zero weight that is already in use raises an Exception.
        """
        with pytest.raises(ValueError):
            relog.add_log_level(name='info', weight=100)

    def test_is_log_level_used(self):
        """`is_log_level_used` returns True for known levels."""
        assert relog.is_log_level_used(relog.INFO)

    def test_is_log_level_used_unknown_level(self):
        """`is_log_level_used` returns False for unknown levels."""
        assert relog.is_log_level_used(9999) is False

    def test_name_of_level_returns_name(self):
        """Function returns the name of a specified logging level."""
        assert relog.name_of_level(relog.INFO) == 'INFO'

    def test_get_log_levels_returns_ints(self):
        """Function returns the list of known logging level integers."""
        l_levels = relog.get_log_levels()
        assert l_levels == relog.core._l_log_level_ints

    def test_get_log_levels_returns_copy(self):
        """
        Function returns a copy of the list of logging level integers, not the
        list itself.
        """
        assert id(relog.get_log_levels()) != id(relog.core._l_log_level_ints)
