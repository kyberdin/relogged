"""
test_logger.py

Logger test cases.

:author:        Stephen Stauts
:created:       5/11/2020   (DD/MM/YYYY)
:copyright:     © 2020 Stephen Stauts. All Rights Reserved.
"""

# Standard Packages
import threading

# Dependency Packages
import pytest

# Local Packages and Modules
from helper import PseudoBaseLogger, PseudoBaseHandler
from conftest import does_not_raise
import relog


class TestBaseLogger:
    """Test cases for BaseLogger functionality."""

    def test_level_sets_level(self, pbl):
        """A logger's level can be set through its property."""
        test_level = relog.FATAL
        assert pbl.level != test_level
        pbl.level = test_level
        assert pbl.level == test_level

    def test_level_setter_non_int(self, pbl):
        """
        An exception is raised when a non-integer level is assigned to the
        logger property.
        """
        with pytest.raises(ValueError):
            pbl.level = 'a string'

    def test_level_setter_unknown(self, pbl):
        """
        An exception is raised when an integer of an unknown level is assigned
        to the logger property.
        """
        with pytest.raises(ValueError):
            pbl.level = 9999

    def test_lock_assigns_lock(self, pbl):
        """The first call to the lock property assigns the lock ID."""
        assert pbl._lock is None
        assert pbl.lock is not None
        assert isinstance(pbl.lock, type(threading.RLock()))

    def test_disabled_when_notset(self):
        """The logger is disabled when its level is NOTSET (default)."""
        pbl = PseudoBaseLogger('Pseudo Logger')
        assert pbl.disabled

    def test_is_enabled_for_when_disabled(self, pbl):
        """The method returns false when the logger is disabled."""
        assert pbl.level == relog.INFO
        pbl.disabled = True
        assert pbl.is_enabled_for(relog.INFO) == False

    def test_is_enabled_for_notset(self, pbl):
        """The method will always return False for the NOTSET level."""
        assert pbl.is_enabled_for(relog.NOTSET) == False

    def test_is_enabled_for_lower_level(self, pbl):
        """
        The method returns False when the specified level is lower than the
        logger's level.
        """
        test_level = relog.DEBUG
        assert pbl.level > test_level
        assert pbl.is_enabled_for(test_level) == False

    def test_is_enabled_for_level_and_up(self, pbl):
        """
        The method returns True when the specified level is the logger's
        current level or higher.
        """
        assert pbl.level != relog.ERROR
        assert pbl.is_enabled_for(pbl.level)
        assert pbl.is_enabled_for(relog.ERROR)

    def test_add_handler_not_basehandler(self, pbl):
        """
        An exception is raised when a object that is not a subclass of
        BaseHandler is passed into the method.
        """
        with pytest.raises(TypeError):
            pbl.add_handler('not a handler', relog.INFO)

    def test_add_handler_unknown_level(self, pbl, pbh):
        """An exception is raised when an unknown level is specified."""
        with pytest.raises(ValueError):
            pbl.add_handler(pbh, 9999)

    def test_add_handler_notset(self, pbl, pbh):
        """
        An exception is raised when attemping to add a handler to the NOTSET
        level.
        """
        with pytest.raises(ValueError):
            pbl.add_handler(pbh, relog.NOTSET)

    def test_add_handler_relevant_levels(self, pbl, pbh):
        """
        Adding a handler to a logger enables the handler for all relevant levels
        (i.e. GTE the specified level).
        """
        test_level = relog.INFO
        pbl.add_handler(pbh, test_level)
        for level, l_handlers in pbl._d_handlers.items():
            if level < test_level:
                assert pbh not in l_handlers
            else:
                assert pbh in l_handlers

    def test_add_handler_level_only(self, pbl, pbh):
        """
        When specified, adding a handler to a logger enables the handler only
        for the given level.
        """
        test_level = relog.WARNING
        pbl.add_handler(pbh, test_level, level_only=True)
        for level, l_handlers in pbl._d_handlers.items():
            if level == test_level:
                assert pbh in l_handlers
            else:
                assert pbh not in l_handlers

    def test_remove_handler_not_basehandler(self, pbl, pbh):
        """
        An exception is raised when an object that is not a subclass of
        BaseHandler is passed as the method argument.
        """
        with pytest.raises(TypeError):
            pbl.remove_handler('not a handler')

    def test_remove_handler_removes_all(self, pbl, pbh):
        """
        A handler is removed from the logger across all logging levels in which
        it exists.
        """
        test_level = relog.INFO
        pbl.add_handler(pbh, test_level)

        def check_presence(exists):
            nonlocal test_level, pbl, pbh
            for level, l_handlers in pbl._d_handlers.items():
                if level >= test_level:
                    if exists:
                        assert pbh in l_handlers
                    else:
                        assert pbh not in l_handlers

        check_presence(True)
        pbl.remove_handler(pbh)
        check_presence(False)

    def test_remove_handler_removes_from_level(self, pbl, pbh):
        """When specified, a handler is only removed from a certain level."""
        test_level = relog.INFO
        pbl.add_handler(pbh, test_level)

        pbl.remove_handler(pbh, level=test_level)

        for level, l_handlers in pbl._d_handlers.items():
            if level == test_level:
                assert pbh not in l_handlers
            elif level > test_level:
                assert pbh in l_handlers

    def test_remove_handlers(self, pbl, pbh):
        """All handlers can be removed from a logger."""
        test_level = relog.INFO
        sh = relog.StreamHandler()
        pbl.add_handler(pbh, test_level)
        pbl.add_handler(sh, test_level)

        for l_handlers in pbl._d_handlers.values():
            assert len(l_handlers)

        pbl.remove_handlers()

        for l_handlers in pbl._d_handlers.values():
            assert len(l_handlers) == 0

    def test_remove_handlers_bad_level(self, pbl, pbh):
        """
        An exception is raised when an invalid level is given to the method.
        """
        with pytest.raises(ValueError):
            pbl.remove_handlers(level=9999)

    def test_remove_handlers_from_level(self, pbl, pbh):
        """All handlers can be removed from a specific level."""
        test_level = relog.INFO
        sh = relog.StreamHandler()
        pbl.add_handler(pbh, test_level)
        pbl.add_handler(sh, test_level)

        pbl.remove_handlers(level=test_level)

        for lvl, l_handlers in pbl._d_handlers.items():
            if lvl == test_level:
                assert len(l_handlers) == 0
            elif lvl > test_level:
                assert len(l_handlers)


class TestGetLoggerFunction:
    """Test cases for the get_logger function."""

    def test_root_logger_default_finds_root(self):
        """
        Calling with no specific logger name does not raise an exception about
        finding no existing logger.
        """
        with does_not_raise(ValueError):
            lgr = relog.get_logger()

    def test_root_logger_default(self):
        """
        Calling with no specific logger name returns the default RootLogger
        instance.
        """
        lgr = relog.get_logger()
        assert isinstance(lgr, relog.logger._RootLogger)
        assert lgr.name == relog.logger.DEFAULT_ROOT_ID

    def test_logger_not_found(self):
        """
        An exception is raised when no logger with a name matching the parameter
        is found.
        """
        with pytest.raises(ValueError):
            lgr = relog.get_logger(name='no such logger')

    def test_custom_root_logger(self, pbl, reset_loggers):
        """
        If a custom root logger has been registered, that logger is returned
        when no name is given instead of the default RootLogger instance.
        """
        relog.register_logger(pbl, as_root=True)
        lgr = relog.get_logger()
        assert lgr.name == pbl.name
        assert relog.logger._custom_root_id == pbl.name

    def test_missing_custom_root_logger(self, reset_loggers):
        """
        If a custom root logger is set but cannot be found in the tracking
        dict (e.g. no strong reference exists anymore), the module removes its
        reference to a custom root log ID and the default is used once more.
        """
        test_logger = PseudoBaseLogger('Test PseudoBaseLogger',
            level=relog.INFO)
        relog.register_logger(test_logger, as_root=True)
        del(test_logger)
        lgr = relog.get_logger()
        assert lgr.name == relog.logger.DEFAULT_ROOT_ID
        assert relog.logger._custom_root_id is None


class TestRegisterLoggerFunction:
    """Test cases for the register_logger function."""

    def test_bad_logger(self):
        """An exception is raised when an invalid Logger object is given."""
        with pytest.raises(TypeError):
            relog.register_logger('not a logger')

    def test_registration(self, pbl, reset_loggers):
        """A new logger can be registered with the module."""
        relog.register_logger(pbl)

        with does_not_raise(ValueError):
            lgr = relog.get_logger(name=pbl.name)

        assert lgr.name == pbl.name

    @pytest.mark.skip(reason=(
        'Already covered by TestGetLoggerFunction.test_custom_root_logger'))
    def test_custom_root_registration(self, pbl, reset_loggers):
        """
        Specifying as_root tracks the associated logger as the custom default
        "root logger" instance.
        """
        pass
