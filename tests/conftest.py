"""
conftest.py

Pytest configurations.

:author:        Stephen Stauts
:created:       24/05/2020   (DD/MM/YYYY)
:copyright:     © 2020 Stephen Stauts. All Rights Reserved.
"""

# Standard Packages
from contextlib import contextmanager
import weakref

# Dependency Packages
import pytest

# Local Packages and Modules
from helper import PseudoBaseHandler, PseudoBaseLogger
import relog


@contextmanager
def does_not_raise(exception):
    """
    Opposite of built-in raises context manager, evaluating that an exception
    did not occur.

    :reference: https://stackoverflow.com/a/42327075
    """
    try:
        yield
    except exception:
        raise pytest.fail(f'Test raised exception: {exception}')


@pytest.fixture
def test_record():
    """A simple LogRecord for ease of testing."""
    yield relog.LogRecord(relog.INFO, 'Testing - 1, 2, 3')


@pytest.fixture
def pbl():
    """The default PseudoBaseLogger used for tests."""
    yield PseudoBaseLogger('Pseudo BaseLogger', level=relog.INFO)


@pytest.fixture
def pbh():
    """The default PseudoBaseHandler used for tests."""
    yield PseudoBaseHandler()


@pytest.fixture
def test_logger(pbh):
    """
    A simple logger class with a suitable testing handler. Both logger and its
    handler are pseudo implementations of their base classes, providing the most
    simplistic representation of a Logger implementation.
    """
    test_logger = PseudoBaseLogger('TestLogger', level=relog.INFO)
    test_logger.add_handler(pbh)
    yield test_logger


@pytest.fixture
def reset_loggers():
    """
    Reset the registered loggers to default settings after completing a test.

    This is a hard-reset of the internal tracking mechanism for loggers. If that
    mechanism changes, this fixture must also be changed to match it, or at
    least result in the same state.
    """
    yield
    relog.logger._custom_root_id = None
    relog.logger._wvd_loggers = weakref.WeakValueDictionary()
    relog.logger._wvd_loggers[relog.logger._root_logger.name] = relog.logger._root_logger
