"""
test_format.py

Formatting test cases.

:author:        Stephen Stauts
:created:       17/05/2020   (DD/MM/YYYY)
:copyright:     © 2020 Stephen Stauts. All Rights Reserved.
"""

# Standard Packages

# Dependency Packages
import pytest

# Local Packages and Modules
import relog


class TestFormatStyle:
    """Test cases for the FormatStyle class."""

    def test_string_repr(self):
        """
        The default string representation of a FormatStyle tuple is its modern
        string.
        """
        assert str(relog.formatting.MSG) == relog.formatting.MSG.modern


class TestFunctions:
    """Test cases for the module-level functions."""

    def test_modernize_legacy_formats(self):
        """
        A format string containing legacy `logging` format strings have all of
        them replaced by their modern equivalents.
        """
        legacy_str = '%(relativeCreated)d %(levelname)s | %(message)s'
        modern_str = '{} {} | {}'.format(
            relog.formatting.RELCR,
            relog.formatting.LVL,
            relog.formatting.MSG)
        assert relog.modernize_legacy_formats(legacy_str) == modern_str

    def test_modernize_mixed_formats(self):
        """
        A format string containing a mix of legacy `logging` format strings and
        modern format strings have all legacy formats replaced by their modern
        equivalents.
        """
        mixed_str = '{} {} | %(message)s'.format(
            relog.formatting.RELCR, relog.formatting.LVL)
        modern_str = '{} {} | {}'.format(
            relog.formatting.RELCR, relog.formatting.LVL, relog.formatting.MSG)
        assert relog.modernize_legacy_formats(mixed_str) == modern_str

    def test_populate_unformatted(self):
        """
        An unformatted string, as would be used by the `str.format` function,
        can be automatically populated by a sequence of format objects.
        """
        unformatted_str = '{} {} | {}'
        formatted_str = '{} {} | {}'.format(
            relog.formatting.RELCR, relog.formatting.LVL, relog.formatting.MSG)
        assert relog.populate_unformatted(unformatted_str,
            relog.formatting.RELCR,
            relog.formatting.LVL,
            relog.formatting.MSG) == formatted_str


class TestFormatter:
    """Test cases for the Formatter class."""

    def test_init_defaults(self):
        """
        Initializing a formatter with no arguments results in a format string
        of just the message.
        """
        fmtr = relog.Formatter()
        assert fmtr.format == relog.formatting.MSG.modern

    def test_init_single_string(self):
        """
        Initializing a formatter with a single string uses that string as the
        format string.
        """
        fmt_str = '{} {} | {}'.format(
            relog.formatting.RELCR, relog.formatting.LVL, relog.formatting.MSG)
        fmtr = relog.Formatter(fmt_str)
        assert fmtr.format == fmt_str

    def test_init_string_with_args(self):
        """
        Initializing a formatter with a string and sequential format style args
        results in a format string that is the string populated with the format
        style args.
        """
        unpopulated_str = '{} {} | {}'
        t_styles = (relog.formatting.RELCR, relog.formatting.LVL,
            relog.formatting.MSG)
        fmt_str = unpopulated_str.format(*t_styles)
        fmtr = relog.Formatter(unpopulated_str, *t_styles)
        assert fmtr.format == fmt_str

    def test_init_bad_string(self):
        """
        Initializing a formatter with a non-string first argument raises an
        exception.
        """
        with pytest.raises(TypeError):
            fmtr = relog.Formatter(0, 1, 2)

    def test_init_format_tracking_default(self):
        """
        Initializing a formatter with defaults only tracks the message format
        style.
        """
        fmtr = relog.Formatter()
        assert fmtr._d_fmts.pop(relog.formatting.MSG.name) == True
        assert all(x is False for x in fmtr._d_fmts.values())

    def test_init_format_tracking_arg_styles(self):
        """
        Initializing a formatter with defaults only tracks the message format
        style.
        """
        fmt_str = '{} {} | {}'.format(
            relog.formatting.RELCR, relog.formatting.LVL, relog.formatting.MSG)
        fmtr = relog.Formatter(fmt_str)
        assert fmtr._d_fmts.pop(relog.formatting.MSG.name) == True
        assert fmtr._d_fmts.pop(relog.formatting.LVL.name) == True
        assert fmtr._d_fmts.pop(relog.formatting.RELCR.name) == True
        assert all(x is False for x in fmtr._d_fmts.values())

    def test_format_record(self):
        """
        A LogRecord is properly formatted into the formatter's format string.
        """
        fmt_str = '{} ({}) | {}'.format(
            relog.formatting.LVL, relog.formatting.LVLNO, relog.formatting.MSG)
        fmtr = relog.Formatter(fmt_str)

        msg = 'Hi!'
        test_str = '{} ({}) | {}'.format(
            relog.name_of_level(relog.INFO), relog.INFO, msg)

        lr = relog.LogRecord(relog.INFO, msg)

        assert fmtr.format_record(lr) == test_str
