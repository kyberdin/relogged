"""
test_handler.py

Handler test cases.

:author:        Stephen Stauts
:created:       17/05/2020   (DD/MM/YYYY)
:copyright:     © 2020 Stephen Stauts. All Rights Reserved.
"""

# Standard Packages
import re
import sys
import threading
import time

# Dependency Packages
import pytest

# Local Packages and Modules
from conftest import does_not_raise
from helper import (
    filter_function_false,
    filter_function_true,
    PseudoBaseHandler,
)
import relog


class TestBaseHandler:
    """
    Test cases for `BaseHandler`. Since this handler class is an abstract class,
    a phony child class of it is used for testing.
    """

    def test_init(self):
        """The class can be initialized without error."""
        with does_not_raise(Exception):
            bh = PseudoBaseHandler()

    def test_init_default_format(self):
        """
        Initializing the handler withing specifying formatting keyword arguments
        creates a default formatter of just the message.
        """
        formatted_str = '{}'.format(relog.formatting.MSG)
        bh = PseudoBaseHandler(format=formatted_str)
        assert bh.format == formatted_str

    def test_init_format_str_and_args(self):
        """
        Specifying a format string and arguments creates an appropriate
        formatter for them.
        """
        unpopulated_str = '{} | {}'
        t_styles = (relog.formatting.LVL, relog.formatting.MSG)
        formatted_str = unpopulated_str.format(*t_styles)

        bh = PseudoBaseHandler(format=unpopulated_str, format_args=t_styles)
        assert bh.format == formatted_str

    def test_init_format_str_only(self):
        """
        Specifying a format string with no arguments creates an appropriate
        formatter it.
        """
        formatted_str = '{} | {}'.format(
            relog.formatting.LVL, relog.formatting.MSG)

        bh = PseudoBaseHandler(format=formatted_str)
        assert bh.format == formatted_str

    def test_init_formatter(self):
        """Specifying a formatter object uses it for the handler."""
        formatter = relog.Formatter(
            '{} | {}', relog.formatting.LVL, relog.formatting.MSG)

        bh = PseudoBaseHandler(formatter=formatter)
        assert bh.format == formatter.format

    def test_init_format_str_precedence(self):
        """
        Specifying a format string and a formatter gives precedence to the
        format string.
        """
        formatted_str = '{} | {}'.format(
            relog.formatting.LVL, relog.formatting.MSG)
        formatter = relog.Formatter(
            '{} ({}) | {}',
            relog.formatting.LVL,
            relog.formatting.LVLNO,
            relog.formatting.MSG,
        )

        bh = PseudoBaseHandler(format=formatted_str, formatter=formatter)
        assert bh.format == formatted_str

    def test_init_format_str_and_args_precedence(self):
        """
        Specifying a formatter, format string, and format arguments gives
        precedence to the format string and arguments.
        """
        unpopulated_str = '{} | {}'
        t_styles = (relog.formatting.LVL, relog.formatting.MSG)
        formatted_str = unpopulated_str.format(*t_styles)
        formatter = relog.Formatter(
            '{} ({}) | {}',
            relog.formatting.LVL,
            relog.formatting.LVLNO,
            relog.formatting.MSG,
        )

        bh = PseudoBaseHandler(
            formatter=formatter, format=unpopulated_str, format_args=t_styles)
        assert bh.format == formatted_str

    def test_repr_no_name(self):
        """Class representation string is formatted correctly."""
        bh = PseudoBaseHandler()
        assert str(bh).replace('Pseudo', '') == '<BaseHandler {}>'.format(
            id(bh))

    def test_repr_with_name(self):
        """Class representation string is formatted correctly."""
        test_name = 'a test name'
        bh = PseudoBaseHandler(name=test_name)
        assert str(bh).replace('Pseudo', '') == '<BaseHandler {}>'.format(
            test_name)

    def test_formatter_sets_formatter(self):
        """
        A handler's formatter can be set to a new formatter.
        """
        formatter = relog.Formatter(
            '{} | {}', relog.formatting.LVL, relog.formatting.MSG)
        bh = PseudoBaseHandler()
        assert bh.format != formatter.format
        bh.formatter = formatter
        assert bh.format == formatter.format

    def test_formatter_bad_formatter(self):
        """
        An exception is raised for attempting to set an invalid formatter.
        """
        bh = PseudoBaseHandler()
        with pytest.raises(TypeError):
            bh.formatter = 'this is not a formatter'

    @pytest.mark.skip(reason='Formatting needs to be updated to support default populations of non-string variables.')
    def test_format_record_formats_all(self):
        """
        Formatting a log record replaces all of its format string syntax
        placeholders with appropriate information for log output.
        """
        bh = PseudoBaseHandler()
        fmt_str = '%(time)s %(created)f %(file)s %(func)s %(lvl)s %(lvlno)s %(line)d %(msg)s ' \
            '%(module)s %(msecs)d %(name)s %(path)s %(pid)d %(proc)s %(relcr)d %(thid)d %(thread)s'
        bh.format = fmt_str
        record = relog.LogRecord(relog.LogLevel.INFO, 'a generic message')
        assert not re.findall(r'\%\((.*?)\)', bh.format_record(record))

    def test_add_filter(self):
        """A filter can be added to a handler."""
        bh = PseudoBaseHandler()
        assert len(bh._l_filters) == 0
        fltr = relog.Filter(filter_function_true)
        bh.add_filter(fltr)
        assert len(bh._l_filters) > 0

    def test_add_filter_bad_filter(self):
        """An exception is raised when a filter argument is invalid."""
        bh = PseudoBaseHandler()
        with pytest.raises(TypeError):
            bh.add_filter('not a filter')

    def test_remove_filter(self):
        """A filter can be removed from the filter list."""
        bh = PseudoBaseHandler()
        fltr = relog.Filter(filter_function_true)
        bh.add_filter(fltr)
        assert len(bh._l_filters) > 0
        bh.remove_filter(fltr)
        assert len(bh._l_filters) == 0

    def test_remove_filter_nonexistent_filter(self):
        """
        Attempting to remove a filter that is not in the list does nothing.
        """
        bh = PseudoBaseHandler()
        with does_not_raise(ValueError):
            bh.remove_filter(relog.Filter(filter_function_true))

    def test_handle_emits_record(self, test_record):
        """A record is emitted when a handler has no filters."""
        bh = PseudoBaseHandler()
        assert not bh.has_records
        bh.handle(test_record)
        assert bh.has_records

    def test_handle_filter_function_called(self, test_record):
        """A filter function is correctly called when handling a LogRecord."""
        called = False
        called_record = None
        called_args = tuple()
        called_kwargs = dict()

        def filter_fn(record:relog.LogRecord, *args, **kwargs):
            nonlocal called, called_record, called_args, called_kwargs
            called = True
            called_record = record
            called_args = args
            called_kwargs = kwargs
            return True

        test_args = (1, 2, 3)
        test_kwargs = {'one': 1, 'two': 3, 'three': 3}

        bh = PseudoBaseHandler()
        bh.add_filter(relog.Filter(filter_fn, test_args, test_kwargs))
        bh.handle(test_record)
        assert called
        assert called_record == test_record
        assert called_args == test_args
        assert called_kwargs == test_kwargs

    def test_handle_emits_on_passing_filters(self, test_record):
        """A LogRecord is emitted when all filters pass."""
        bh = PseudoBaseHandler()
        bh.add_filter(relog.Filter(filter_function_true))
        assert not bh.has_records
        bh.handle(test_record)
        assert bh.has_records

    def test_handle_does_not_emit_on_filter_fail(self, test_record):
        """A LogRecord is not emitted when at least one Filter fails."""
        bh = PseudoBaseHandler()
        bh.add_filter(relog.Filter(filter_function_true))
        bh.add_filter(relog.Filter(filter_function_false))
        assert not bh.has_records
        bh.handle(test_record)
        assert not bh.has_records


class TestStreamHandler:
    """Test cases for the `Streamhandler` class functionality."""

    def test_init(self):
        """The class can be initialized without error."""
        with does_not_raise(Exception):
            sh = relog.StreamHandler()

    def test_init_invalid_stream(self):
        """
        An exception is raised when an invalid stream is given as the stream
        source.
        """
        with pytest.raises(ValueError):
            sh = relog.StreamHandler(stream='devnull')

    def test_init_default(self):
        """
        Class is initialized with expected default attribute values when no
        arguments are given.
        """
        sh = relog.StreamHandler()
        assert sh.stream == sys.stderr
        assert sh.lock is relog.get_lock('stderr')

    def test_init_stdout(self):
        """
        Class is initialized with stdout-related attribute values when it is
        given as the stream argument.
        """
        sh = relog.StreamHandler(stream=sys.stdout)
        assert sh.stream == sys.stdout
        assert sh.lock is relog.get_lock('stdout')

    def test_init_default_name_reflects_stream(self):
        """
        The default name for the handler reflects its assigned stream.
        """
        stream = sys.stdout
        stream_name = relog.StreamHandler.name_for_stream(stream)
        sh = relog.StreamHandler(stream=stream)
        assert sh.name == '{}-{}'.format(stream_name, id(sh))

        stream = sys.stderr
        stream_name = relog.StreamHandler.name_for_stream(stream)
        sh = relog.StreamHandler(stream=stream)
        assert sh.name == '{}-{}'.format(stream_name, id(sh))

    def test_init_specific_name(self):
        """
        Specifying a name for the handler uses that name.
        """
        test_name = 'MyStreamHandler'
        sh = relog.StreamHandler(stream=sys.stderr, name=test_name)
        assert sh.name == test_name

    def test_is_valid_stream(self):
        """
        The function properly validates the supported streams.
        """
        assert relog.StreamHandler.is_valid_stream(sys.stdout)
        assert relog.StreamHandler.is_valid_stream(sys.stderr)

    def test_is_valid_stream_unsupported_stream(self):
        """
        The function properly invalidates unsupported streams or non-stream
        objects.
        """
        assert not relog.StreamHandler.is_valid_stream('not a stream')

    def test_name_for_stream(self):
        """
        The function properly returns the name associated to supported streams.
        """
        assert relog.StreamHandler.name_for_stream(sys.stdout) == 'stdout'
        assert relog.StreamHandler.name_for_stream(sys.stderr) == 'stderr'

    def test_name_for_stream_unsupported_stream(self):
        """
        The function returns no stream neam for unsupported stream.
        """
        assert relog.StreamHandler.name_for_stream('not a stream') == 'unknown'

    @pytest.mark.skip(reason='Not sure how to implement this')
    def test_flush(self):
        """
        The stream is flushed properly.
        """
        pass

    def test_emit(self, capsys):
        """Emit writes a log record to the configured stream."""
        sh = relog.StreamHandler()
        test_string = 'this is a test string'
        sh.emit(relog.LogRecord(relog.INFO, test_string))
        assert capsys.readouterr().err.rstrip('\n') == test_string

    def test_emit_lock(self, capsys):
        """Emit writes a log record only after the stream lock is released."""
        sh = relog.StreamHandler()
        test_string = 'this is a test string'
        record = relog.LogRecord(relog.INFO, test_string)

        def hold_lock(lock, duration_s):
            """
            Function used by a thread to acquire the stream lock for a time,
            preventing the handler from writing output.
            """
            with lock:
                start_time = time.time()
                while (time.time() - start_time) < duration_s:
                    time.sleep(duration_s / 5)

        hold_duration_s = 0.1
        thd = threading.Thread(target=hold_lock, args=(sh.lock, hold_duration_s))

        output = ''
        start_time = time.time()
        thd.start()
        sh.emit(record)

        # Wait for output to appear
        while not output:
            output = capsys.readouterr().err

        wait_time_s = time.time() - start_time
        thd.join()

        assert output.rstrip('\n') == test_string

        # Assess the passed time before stream output was captured, with
        # 25% tolerance to account for processing time between time captures.
        assert wait_time_s >= (hold_duration_s * 0.75)
        assert wait_time_s <= (hold_duration_s * 1.25)
