"""
helper.py

Helper functionality for tests.

:author:        Stephen Stauts
:created:       24/05/2020   (DD/MM/YYYY)
:copyright:     © 2020 Stephen Stauts. All Rights Reserved.
"""

# Standard Packages

# Dependency Packages

# Local Packages and Modules
import relog


class PseudoBaseHandler(relog.BaseHandler):
    """Basic child class to test abstract `BaseHandler` functionality."""

    def __init__(self, **kwargs):
        """Initialize the class attribute(s)."""
        super().__init__(**kwargs)
        self.l_records = list()

    @property
    def lock_id(self) -> str:
        """Specific ID of the threading lock to use for the handler."""
        return self.__class__.__name__

    @property
    def has_records(self) -> bool:
        """The handler contains emitted log records."""
        return bool(len(self.l_records))

    def emit(self, record:relog.LogRecord):
        """
        Emit the record by placing it into the internally tracked list of
        records.
        """
        self.l_records.append(self.format_record(record))

    def flush(self):
        """Make the abstract inheritance happy."""
        pass


class PseudoBaseLogger(relog.BaseLogger):
    """Basic child class to test abstract `BaseLogger` functionality."""

    @property
    def lock_id(self) -> str:
        """Specific ID of the threading lock to use for the logger."""
        return self.__class__.__name__


def filter_function_false(record:relog.LogRecord, *args, **kwargs):
    """Filter function that returns a failed filter check."""
    return False


def filter_function_true(record:relog.LogRecord, *args, **kwargs):
    """Filter function that returns a successful filter check."""
    return True
