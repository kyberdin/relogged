"""
version.py

Version information for the module.

:author:        Stephen Stauts
:created:       09/05/2020   (DD/MM/YYYY)
:copyright:     © 2020 Stephen Stauts. All Rights Reserved.
"""

# Standard Packages

# Dependency Packages

# Local Packages and Modules


__version__ = '0.1.0'
