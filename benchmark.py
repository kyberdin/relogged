"""
benchmark.py
Stephen Stauts

Benchmark executor for testing relog function speeds versus the built-in
logging package equivalent.

Usage:
    benchmark.py [options] [--iterations CYCLES] [--print]

Options:
    -n, --num-cycles CYCLES     Number of times to run the functionality being
                                benchmarked.
                                [Default: {cycles}]

    --simple                    Log only the message contents.

    --print                     Print the logging output to the console.

    -h, --help                  Show this text.
"""
# file:          benchmark.py
# author:        Stephen Stauts
# created:       22/10/2020   (DD/MM/YYYY)
# copyright:     © 2020 Stephen Stauts. All Rights Reserved.


# Standard Packages
import logging
import os
import sys
import time

# Dependency Packages
import docopt

# Local Packages and Modules
import relog


# Update the version as tracked in the package file
__doc__ = __doc__.format(**{
    'cycles': 10000,
})


def streaming_benchmark(fmt:str, msg:str, num_cycles:int):
    """Benchmark test for StreamHandler output."""

    def logging_stream_info_benchmark(logger, msg:str):
        logger.info(msg)

    def relogged_stream_info_benchmark(logger, msg:str):
        logger.info(msg)

    logging.basicConfig(
        level=logging.INFO,
        stream=sys.stderr,
        format=fmt,
    )

    logging_root_logger = logging.getLogger()

    # The root logger has already pointed to STDERR by the time DEVNULL is
    # assigned to sys.stderr; workaround it by removing the original handler
    # and adding it again to point to DEVNULL.
    relog.logger._root_logger.remove_handlers()
    sh = relog.StreamHandler(format=relog.modernize_legacy_formats(fmt))
    relog.logger._root_logger.add_handler(sh, relog.INFO)
    relog_root_logger = relog.get_logger()

    for i in range(num_cycles):
        logging_stream_info_benchmark(logging_root_logger, msg)
        relogged_stream_info_benchmark(relog_root_logger, msg)


def main():
    """
    Main script routine.
    """
    d_cli_args = docopt.docopt(__doc__)

    if d_cli_args['--simple']:
        fmt = '%(message)s'
    else:
        fmt = '%(asctime)s %(levelname)s (%(levelno)s) | %(relativeCreated)d: %(message)s'

    if not d_cli_args['--print']:
        devnull = open(os.devnull, 'w')
        sys.stderr = devnull

    test_string = 'Breate, breathe in the air...don\'t be afraid to care'
    num_cycles = int(d_cli_args['--num-cycles'])

    start_time = time.time()

    print('Running streaming benchmark')

    streaming_benchmark(fmt, test_string, num_cycles)

    print('Benchmark completed in {:.03f} seconds'.format(time.time() - start_time))


if __name__ == '__main__':
    try:
        main()
    except docopt.DocoptExit:
        print('ERROR: Command line contained one or more invalid arguments.')
        print(__doc__)
        sys.exit(1)
    except:
        raise
